﻿using System.Collections.Generic;
using UnityEngine;
using Game.IS;

public class Player : MonoBehaviour, ItemsIDStorage
{
    [SerializeField] float speed = 0;
    public RightJoystick Rotate_Joystick;
    public LeftJoystick Move_Joystick;
    Rigidbody2D rb2d;

    public List<ushort> ItemsID { get; set; }

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    private void FixedUpdate()
    {
        if (Rotate_Joystick.GetInputDirection() != Vector3.zero)
        {
            Vector3 diff = Rotate_Joystick.GetInputDirection();
            diff.Normalize();

            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
        if (Move_Joystick.GetInputDirection() != Vector3.zero)
        {
            transform.localPosition += (Move_Joystick.GetInputDirection() * speed);
        }
    }
}
