﻿using UnityEngine;
using Game.IS;

public class GameManager : MonoBehaviour
{
    public static GameManager Get { get; private set; }
    private void Awake()
    {
        Get = this;
    }

    public ItemDataBaze UsedItemDataBaze;
}
