﻿using System.Collections.Generic;
using UnityEngine;
using Game.IS;

public class Inventory : MonoBehaviour, ItemsIDStorage
{
    GameManager manager;
    public List<ushort> ItemsID { get => _itemsID; set => _itemsID = value; }
    [SerializeField]List<ushort> _itemsID;
    [SerializeField]bool IsGridCountStatic = false;
    InventoryGrid[] inventoryGrids;
    private void Awake()
    {
        manager = GameManager.Get;
        if (IsGridCountStatic)
        {
            inventoryGrids = GetComponentsInChildren<InventoryGrid>();
            if (_itemsID.Count == inventoryGrids.Length)
            {
                for (int i = 0; i < inventoryGrids.Length; i++)
                {
                    inventoryGrids[i].SetItemWhithID(_itemsID[i],manager.UsedItemDataBaze);
                }
            }
            else
                Debug.LogError("количество ячеек инвенторя должно совпадать с количеством предметов в нём");
        }
    }

}
