﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Game.IS
{
    [System.Serializable]
    public struct Item
    {
        public Sprite image;
    }
    public interface ItemsIDStorage
    {
        List<ushort> ItemsID { get; set; }
    }
#if UNITY_EDITOR
    /*
    [CustomEditor(typeof(ItemDataBaze))]
    class ItemDataBaze_Editor : Editor
    {
        public override void OnInspectorGUI()
        {
            var target = this.target as ItemDataBaze;
            if (target.items == null)
                target.items = new List<Item>();
            int currectItemElement;
            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("<<"))
                    itemsList.currecntItem = itemsList.GetPrev();
                itemsList.currecntItem = itemsList.GetElement(EditorGUILayout.IntField(itemsList.currect_ItemIndex));
                if (GUILayout.Button(">>"))
                    itemsList.currecntItem = itemsList.GetNext();
                if (GUILayout.Button("Remove"))
                    itemsList.Remove();
                if (GUILayout.Button("add"))
                    itemsList.AddElement();
                if (GUILayout.Button("save"))
                    itemsList.Save();
            }
            EditorGUILayout.EndHorizontal();
        }
    }
    */
#endif
}
