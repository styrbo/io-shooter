﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.IS
{
    [CreateAssetMenu(menuName = "Game/IS/ItemDataBaze", fileName = "new ItemDataBaze")]
    public class ItemDataBaze : ScriptableObject
    {
        public List<Item> items;
    }
}
