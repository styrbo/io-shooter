﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Game.IS;

public class InventoryGrid : MonoBehaviour
{
    public ushort Id;
    Image gridImage;
    private void Awake()
    {
        gridImage = GetComponent<Image>();
    }
    public void SetItemWhithID(ushort id,ItemDataBaze usedDataBaze)
    {
        gridImage.sprite = usedDataBaze.items[id].image;
    }
}
